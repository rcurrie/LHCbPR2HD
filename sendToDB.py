#!/usr/bin/env python

import sys
import json
import logging
import zipfile
import os
import subprocess
from optparse import OptionParser


logger = logging.getLogger('sendToDB.py')

diracStorageElementName = 'StatSE'

diracStorageElementFolder = '/lhcb/prdata/zips'


def sendViaDiracStorageElement(zipFile):
    head, tailzipFile = os.path.split(zipFile)

    # from DIRAC.Core.Base.Script import initialize
    # initialize(ignoreErrors=True, enableCommandLine=False)

    # from DIRAC.Resources.Storage.StorageElement import StorageElement
    # statSE = StorageElement(diracStorageElementName)

    # log = statSE.putFile(
    #     {os.path.join(diracStorageElementFolder, tailzipFile): zipFile})
    # logger.info('{0}'.format(log))
    lfn = os.path.join(diracStorageElementFolder, tailzipFile)
    result = subprocess.check_output(["dirac-dms-add-file", "-ddd", lfn,
                                      zipFile, diracStorageElementName])
    if result.count("Successfully uploaded ") == 1:
        logger.info("Uploaded {0}".format(lfn))
    else:
        logger.error("Fail to upload {0}".format(lfn))


def run(zipFile, ssss):

    ch = logging.StreamHandler()
    ch.setLevel(level=logging.WARNING)

    if not ssss:
        logging.root.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if not zipfile.is_zipfile(zipFile):
        logger.error(
            'Given object is not a valid zip file, please give a valid one, aborting...')
        return

    # checking if the zip contains what it should contains
    try:
        unzipper = zipfile.ZipFile(zipFile)
        dataDict = json.loads(unzipper.read('json_results'))

        for atr in dataDict['JobAttributes']:
            if atr['type'] == 'File':
                unzipper.read(atr['filename'])

    except Exception, e:
        logger.error(e)
        logger.error('Aborting...')
        return

    logger.info('Given zip file is valid, sending to database...')

    sendViaDiracStorageElement(zipFile)
    return


def main():
    # this is used for checking
    needed_options = 3

    description = """The program needs all the input arguments(options in order to run properly)"""
    parser = OptionParser(usage='usage: %prog [options]',
                          description=description)
    parser.add_option('-s', '--send-results',
                      action='store', type='string',
                      dest='zipFile', help='Zip file with results to be pushed to database')
    parser.add_option("-q", "--quiet", action="store_true",
                      dest="ssss", default=False,
                      help="Just be quiet (do not print info from logger), optional")

    if len(sys.argv) < needed_options:
        parser.parse_args(['--help'])
        return

    options, args = parser.parse_args()

    run(options.zipFile, options.ssss)

if __name__ == '__main__':
    main()
