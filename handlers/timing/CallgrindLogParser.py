import re
import os


"""
The CallgrindLogParser parses a callgrind logfile which was generated with

file=callgrind.out.<pid>.1
callgrind_annotate --inclusive=yes ${file}  > ${file}.anno

The instance takes an argument algos from which it generates a function string
'<algorithm>::operator()' which is used to select those lines from the log file
with the corresponding algorithm invocations. The numbers extracted are all
passed to the data structure which is returned. In addition some more values
are caclulated such as cache misses, ratios etc. 
"""


class CallgrindLogParser():

    def __init__(self,
                 dir,
                 algos=[],
                 filepat=r'callgrind.out.[0-9]+.[0-9].anno'):
        self.directory = dir
        self.algos = algos
        self.callgrindfilepat = filepat
        self.callgrindvalues = [['ir', 'instruction fetch'],
                                ['dr', 'data read access'],
                                ['dw', 'data write access'],
                                ['i1mr', 'l1 instruction fetch miss'],
                                ['d1mr', 'l1 data read miss'],
                                ['d1mw', 'l1 data write miss'],
                                ['ilmr', 'last level data instr. fetch miss'],
                                ['dlmr', 'last level data read miss'],
                                ['dlmw', 'last level data write miss'],
                                ['bc', 'conditional branch'],
                                ['bcm', 'mispredicted conditional branch'],
                                ['bi', 'indirect branch'],
                                ['bim', 'mispredicted indirect branch'],
                                ['ifp32x1', 'scalar f32 instructon'],
                                ['ifp64x1', 'scalar f64 instructon'],
                                ['ifp32x2', 'simdx2 f32 instructon'],
                                ['ifp64x2', 'simdx2 f64 instructon'],
                                ['ifp32x4', 'simdx4 f32 instructon'],
                                ['ifp64x4', 'simdx4 f64 instructon'],
                                ['ifp32x8', 'simdx8 f32 instructon']]
        self.callgrindfields = [x[0] for x in self.callgrindvalues] + \
            ['file', 'funall', 'funname']
        self.callgrindmetrics = {}

    def extractCallgrindMetrics(self):
        pat = re.compile(self.callgrindfilepat)
        callgrindfile = filter(pat.match, os.listdir(self.directory))
        if len(callgrindfile) == 0:
            print 'Error: cannot find callgrind log file in %s' %\
                  str(os.path.realpath(os.curdir))
        elif len(callgrindfile) > 1:
            print 'Error: found multiple log files matching pattern %s in %s' %\
                  (self.callgrindfilepat, str(os.listdir(os.curdir)))

        searchops = ['%s::operator()' % x['algo_class'] for x in self.algos]
        oppatstr = r'[A-Z,a-z,0-9]+::operator\(\)'
        oppat = re.compile(r':' + oppatstr)
        oppatbeg = re.compile(r'^' + oppatstr)
        algpat = re.compile(r'[A-Z,a-z,0-9]+')
        fh = open(os.path.join(self.directory,callgrindfile[0]))
        for line in fh.readlines():
            match = oppat.findall(line)
            if len(match) and match[0][1:] in searchops:
                lines = line.split()
                linesvals = lines[:20]
                linesvals = map(lambda x: int(x.replace(',', '').
                                replace('.', '0')), linesvals)
                metricvals = linesvals + ''.join(lines[19:]).split(':', 1)
                metricvals.append(oppatbeg.findall(metricvals[-1])[0])
                algname = algpat.findall(metricvals[-1])[0]
                metricdict = dict(zip(self.callgrindfields, metricvals))
                if algname in self.callgrindmetrics:
                    for metr in self.callgrindvalues:
                        value = metr[0]
                        if self.callgrindmetrics[algname][value] != \
                           metricdict[value]:
                            print """Error: for algorithm %s two different
                                  values found for callgrind metric %s: %d, %d
                                  """ % (algname, value, metricdict[value],
                                         self.callgrindmtrics[algname][value])
                else:
                    self.callgrindmetrics[algname] = metricdict
        fh.close()
        return (self.callgrindmetrics, self.callgrindvalues)

    def run(self):
        self.extractCallgrindMetrics()

if __name__ == '__main__':
    CallgrindLogParser().run()

# ir        Instruction Fetch
# dr        Data Read Access
# dw        Data Write Access
# l1mr      L1 Instr. Fetch Miss
# d1mr      L1 Data Read Miss
# d1mw      L1 Data Write Miss
# ilmr      LL Instr. Fetch Miss
# dlmr      LL Data Read Miss
# dlmw      LL Data Write Miss
# bc        Conditional Branch
# bcm       Mispredicted conditional Branch
# bi        indirect Branch
# bim       mispredicted indirect Branch
# ifp32x1   scalar f32 Inst
# ifp64x1   scalar f64 instr
# ifp32x2   simdx2 f32 instr
# ifp64x2   simdx2 f64 instr
# ifp32x4   simdx4 f32 instr
# ifp64x4   simdx4 f64 instr
# ifp64x8   simdx6 f32 instr
# l1m       = i1mr + d1mr + d1mw                                L1 Miss Sum
# llm       = ilmr + dlmr + dlmw                                LastLevelMissSum
# bm        = bim + bcm                                         Mispredic Branch
# cest      = ir + 10 * bm + 10 * l1m + 100 * llm               Cycle estimation
# fp32      = ifp32x1 + 2 * ifp32x2 + 4 * ifp32x4 + 8 * ifp32x8 f32 op
# fp64      = ifp64x1 + 2 * ifp64x2 + 4 * ifp64x4               f64 op
# vfp128    = 4 * ifp32x4 + 2 * ifp64x4                         simd 128 fp op
# vfp256    = 8 * ifp32x8 + 4 * ifp64x4                         simd 256 fp op
# vfp       = 2 * ifp32x2 + vfp128 + vfp256                     simd fp op
# sfp       = ifp32x1 + ifp64x1                                 scalar fp op
# flop      = fp32 + fp64                                       fp op (FLOP)
#
# btot      = bc + bi                                           total branches
# itot      = ir + dr + dw                                      total instructio
