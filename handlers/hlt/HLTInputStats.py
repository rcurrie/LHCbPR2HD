#!/bin/env python
import json, os, sys, ROOT
from array import array


def studyInputStats(filename):
    
    rootFile = ROOT.TFile( filename )
    if rootFile.IsZombie():
        print("ROOT file is Zombie")
        sys.exit()
    tree = rootFile.Get("InputStats")
    if not tree:
        print("Not present in file")
        sys.exit()

    results = []
    for data in tree:
        results.append( {'inputrate' : data.inputrate,
                         'bunches' : data.bunches} )

    return results


if __name__ == "__main__":

    filename = sys.argv[1]

    results = studyInputStats(filename)

    JSONDIR = os.path.abspath(os.path.dirname(filename))

    for data in results: #only one entry
        with open(os.path.join(JSONDIR, "InputStats.json"), 'w') as _file:
            print("writing: " % _file.name)
            json.dump(data, _file)

