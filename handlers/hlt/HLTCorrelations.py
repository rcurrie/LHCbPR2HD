#!/usr/bin/python
import os,sys,math,re,ROOT
import random
sys.argv.append( '-b' )
from array import array
sys.path.insert(0, 'scripts')
sys.path.insert(1, 'scripts')
######### SET ME ##########
maxEvt = -1
filename = sys.argv[1]
######### SET ME ##########

from StreamDefs import (exclude,_Streams,prescales)
print exclude
ChosenStream  = _Streams["AllStreams"]

rootFile = ROOT.TFile( filename )
rootFile.ls()
treeHlt1 = rootFile.Get( "TupleHlt1/HltDecReportsTuple" )
treeHlt2 = rootFile.Get( "TupleHlt2/HltDecReportsTuple" )
branches = treeHlt2.GetListOfBranches()
decisions = {}
lineName = None
dm = {}
branch_names = []

for branch in branches:
    branchName = branch.GetName()
    if "Decision" in branchName and branchName.replace("Decision","") not in exclude:
        branchName = branchName.replace("Decision","")
        print branchName
        branch_names.append(branchName)
        decisions[branchName] = {}
        print ChosenStream
        for Stream in ChosenStream:
            decisions[branchName][Stream[1]] = 0.0

##########################################################
######### EVENT LOOP #####################################
##########################################################

variables = {}
for line in decisions.keys():
    variables[line] = array('I',[0])
    treeHlt2.SetBranchAddress( line+"Decision", variables[line] )
iEntry = 0

while treeHlt2.GetEntry(iEntry):
    iEntry+=1
    if maxEvt > 0 and iEntry > maxEvt: break
    if iEntry % 10000 == 0:print 'processed %s/%s' %(iEntry,treeHlt2.GetEntries())
    ## set prescales
    for decision in decisions:
        if decision in prescales.keys():
            if random.uniform(0,1) > prescales[decision]:
                variables[decision][0] = 0
    ## by stream
    StreamDecs = {}
    for Stream in ChosenStream:
        StreamDecs[Stream[0]] = 0
    for decision in decisions:
        decThisLine = variables[decision][0] 
        for Stream in ChosenStream:
            if decThisLine == 1 and re.match(Stream[0], decision, flags=0):
                StreamDecs[Stream[0]] = 1
    # by line
    
    for decision in decisions:
        decThisLine = variables[decision][0] 
        for Stream in ChosenStream:
            #print "------"
            #print 'line = %s' %decision
            #print 'line dec = %s' %decThisLine
            #print "StreamDecs:"
            #print StreamDecs
            if decThisLine == 1 and StreamDecs[Stream[0]] == 1:
                decisions[decision][Stream[1]] += 1
    
##########################################################
######### END EVENT LOOP #####################################
##########################################################

# convert from events to rate                
for Hlt2LineName,Hlt2LineStats in decisions.iteritems():
    for key,value in Hlt2LineStats.iteritems():
        Hlt2LineStats[key] = 1.e6*float(value)/float(treeHlt1.GetEntries()) ## rate

############ finally, write out everything to json files
import json
print 'writing json files'
json.dump(decisions, open("decisions.json",'w'))
