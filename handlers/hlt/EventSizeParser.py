#!/bin/env python
import ROOT
from os import path


def eventSizeParser(filename):
    """ Class for parsing the information for the EventSize table
    Args:
        folder (str): folder where the input is and where the output will be stored
        filename (str): filename containing the output from the hlt1 step of the ratetests
    """
    
    rootFile = ROOT.TFile(filename)
    treeRB = rootFile.Get("TupleHlt1")
    eventSizes = {}

    folder = path.dirname(filename)
    eventFileName = path.join(folder, "EventSize.root")
    eventFile = ROOT.TFile(eventFileName, "recreate")

    names = {46:"HLT1",
             87:"Full",
             88:"Turbo",
             90:"Turbocalib"}


    for RB, name in names.iteritems():
        eventSizes['RB%s' % RB] = {'name' : name}
        
        for RawEventType in ["Full", "Turbo", "persistReco"]:
            c = ROOT.TCanvas('RB%s_%s'  % (RB, RawEventType))
            histname = RawEventType + str(RB)
            hist = ROOT.TH1F(histname, "Routing bit " + str(RB), 100, 0, 200)
            treeRB.Draw(RawEventType + "RB%s*1.e-3>>%s" % (RB, histname), RawEventType + "RB%s>0" % RB)
            hist.SetXTitle(RawEventType+ "event size (kB)")
            hist.Draw("HIST")
            hist.SetFillColor(ROOT.kYellow)
            hist.SetLineColor(ROOT.kYellow+1)
            eventSizes['RB%s_%s_mean' % (RB, RawEventType) ] = hist.GetMean()
            eventSizes['RB%s_%s_err' % (RB, RawEventType) ] = hist.GetMeanError()

            eventFile.cd()
            c.Write()
            eventFile.Write()

    eventFile.Write()
    eventFile.Close()

    results = {}
    results['eventSizes'] = eventSizes
    results['eventFileName'] = eventFileName

    return results

if __name__ == "__main__":

    eventSizeParser(sys.argv[1])


