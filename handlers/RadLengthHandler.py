#########################################################################
##    Collects results for RadLength test which includes a detector    ##
##    scan and separate VELO region scan.                              ##
##                                                                     ##
##    @author  :   K. Zarebski                                         ##
##    @date    :   last modified 2017-12-07                            ##
#########################################################################

import os
import re
import json
from BaseHandler import BaseHandler
import logging

class RadLengthHandler(BaseHandler):	
    def __init__(self, debug='INFO'):
        super(self.__class__,self).__init__()

    def collectResults(self, directory):
        _rad_length_detec_scan_dir = ('RadLengthDetectorScan', os.path.join(directory, 'RadLengthDetectorScan/root_files/RadLengthSubDetectorPlots.root'))
        _rad_length_velo_scan_dir  = ('RadLengthVeloScan', os.path.join(directory, 'RadLengthVeloScan/root_files/RadLengthVeloScan.root')) 

        for file_ in [_rad_length_detec_scan_dir, _rad_length_velo_scan_dir]:
            self.saveFile(*file_)
