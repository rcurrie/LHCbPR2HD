import os, re
from BaseHandler import BaseHandler

class PerfHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()

    REG="\s*(\d+\.\d+)\%\s+(\d+\.\d+)\%\s+(\w+)\s+(\w+\.\w+)\s+\[\.\]\s+(\w+\:\:\w+)$"
    # 53.76%     0.12%  python           libG4tracking.so                             [.] G4TrackingManager::ProcessOneTrack

    def collectResults(self, directory):
        self.saveFile('perf.lbr' , os.path.join(directory,'perf.lbr.txt') )
        with open(os.path.join(directory,'perf.lbr.txt')) as f:

            for line in f.readlines():
                metric = re.match(self.REG, line)

                if metric != None and float(metric.group(2)) > 0.0:
                    self.saveFloat(metric.group(5)+"_self",
                                   metric.group(2),
                                   description="time spent by function itself in %",
                                   group="self")
                    self.saveFloat(metric.group(5)+"_children",
                                   metric.group(1),
                                   description="time spent by children of function in %",
                                   group="children")
