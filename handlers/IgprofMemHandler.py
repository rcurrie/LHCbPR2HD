import os, re
from BaseHandler import BaseHandler

class IgprofMemHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()

    REG="\s*(\d+\.\d+)\s+([\d\']+)\s+([\d\']+)\s+(\S+)\s+\[\d+\]$"
    # 21.13   41'314'528          968  _ZN9RichHpdQE12setAnHpdQEenEiRKSt6vectorIdSaIdEES4_ [81]

    def parseIgprof(self, directory, filename, prof_type):
        start = False
        with open(os.path.join(directory, filename)) as f:
            for line in f.readlines():
                if "Flat profile (self" not in line and start is False:
                    continue
                if "Call tree profile" in line:
                    break
                start = True
                metric = re.match(self.REG, line)
                if metric != None:
                    self.saveFloat(metric.group(4)+"_percent",
                                   metric.group(1),
                                   description="memory " + prof_type + " in %",
                                   group="mem" + prof_type + "_percent")
                    self.saveInt(metric.group(4)+"_byte",
                                 metric.group(2).replace("'",""),
                                 description="memory "+ prof_type + " in bytes",
                                 group="mem" + prof_type +"_byte")

    def collectResults(self, directory):
        self.parseIgprof(directory, "igout.mp.live.txt", "leak")
        self.parseIgprof(directory, "igout.mp.total.txt", "alloc")
        self.saveFile('igprof_MemLive' , os.path.join(directory,'igout.mp.live.txt') )
        self.saveFile('igprof_MemTotal' , os.path.join(directory,'igout.mp.total.txt') )
