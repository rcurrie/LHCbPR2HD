import os, sys, re
from BaseHandler import BaseHandler
from xml.etree.ElementTree import ElementTree
from xml.parsers.expat import ExpatError
from hlt import HLTRateParser
import subprocess
import shutil

class HLTRateHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()
        self.finished = False
        self.results = []


    RE_NBWERROR = "([0-9\.]+)\+\-([0-9\.]+)"
    def _parseValError(self, valstr):
        """ Parse a number in the format: 150.00+-35.71 """
        m = re.match(self.RE_NBWERROR, valstr)
        if m != None:
            return (m.group(1), m.group(2))
        else:
            return (None, None)


    def _parseHLTRateLog(self, filename, directory):
        """ Parse the log of the HLT rate file to send to the DB """

        # extracted rate table from the log
        rateTable = []

        with open(filename) as f:
            # Now iterating on the file...
            for l in f.readlines():
                # Skipping till the start of the rates summary
                if "HLT rate summary starts here" in l:
                    rateTable.append(l)
                    continue
                if len(rateTable) > 0:
                    # Using the rate table length to know whether we've
                    # found the start line already
                    rateTable.append(l)
                if "HLT rate summary ends here" in l:
                    break

        # Parsing the rate table to extract the figures
        from hlt import HLTRateParser
        data = HLTRateParser.parseHLTRateList("".join(rateTable))

        globalGroup = "HTLRate_Global"
        globalStatsKeys = ['Hlt1Lines', 'Hlt2Lines', 'nbevents', 'ratesAssume']
        for k in globalStatsKeys:
            self.saveFloat(k, float(data[k]), group=globalGroup)

        globalStatsWithErrKeys = ['TurcalRate', 'TurboRate', 'Hlt2GlobalRate', 'Hlt1GlobalRate', 'FullRate' ]
        for k in globalStatsWithErrKeys:
            self.saveFloat(k, float(data[k][1]), group=globalGroup)
            self.saveFloat(k + "_error", float(data[k][2]), group=globalGroup)

        # for each line we have:
        #  <line number>, <line name>, <incl. rate>, <incl. rate error>, <excl. rate>, <excl rate error> ]
        # ['1', 'Hlt1TrackMVA', '150.00', '35.71', '80.00', '27.13']

        def saveStatsLine(prefix, statsGroup, data):
            """ Format the stats for LHCbPR """
            (num, name, irate, irateerr, erate, erateerr) = data

            getname = lambda par: "_".join([ prefix, name, par])

            self.saveFloat(getname("nb") , int(num), group=statsGroup)
            self.saveFloat(getname("Incl_rate") , float(irate), group=statsGroup)
            self.saveFloat(getname("Incl_rate_err") , float(irateerr), group=statsGroup)
            self.saveFloat(getname("Excl_rate") , float(erate), group=statsGroup)
            self.saveFloat(getname("Excl_rate_err") , float(erateerr), group=statsGroup)

        for d in data["Hlt1RegexStats"]:
            saveStatsLine("HLT1Regex", "HLTRate_HLT1RegexLineStats", d)

        for d in data["Hlt2RegexStats"]:
            saveStatsLine("HLT2Regex", "HLTRate_HLT2RegexLineStats", d)

        for d in data["Hlt1Stats"]:
            saveStatsLine("HLT1", "HLTRate_HLT1LineStats", d)

        for d in data["Hlt2Stats"]:
            saveStatsLine("HLT2", "HLTRate_HLT2LineStats", d)

        self.saveFile("tuples.root", os.path.join(directory,'tuples.root'),  group="HTLRate_Global")

    def _publishStaticHTML(self, directory, project, version):
        """ Generate the static HTML files for Mika and copies them to AFS """
        # This is a hack to boostrap the the process, until LHCbPR
        # has the required functionality
        wwwDir = os.environ.get("LHCBPR_WWW")
        if wwwDir == None:
            raise Exception("No web dir defined, will not run extraction")

        if not os.path.exists(wwwDir):
            raise Exception("LHCBPR_WWW defined but does not exist")

        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception("No web dir on EOS defined, will not run extraction")

        # Getting the repo with the transform code
        print "Publish HTML files to AFS and EOS"
        try:
            import shutil
            shutil.rmtree("HLTRateScripts")
        except:
            pass

        if not os.path.exists("HLTRateScripts"):
            rc = subprocess.call("git clone --quiet ssh://git@gitlab.cern.ch:7999/lhcb-core/HLTRateScripts.git", shell=True)

        # Now calling the scripts and creating the directory
        import datetime
        i = datetime.datetime.now()
        dirname = str(version) + "_" + i.strftime("%Y-%m-%dT%H:%M:%S")

        # Checking if there is a JobSetting.txt file
        settingsfile = os.path.join(directory, "JobSettings.txt")
        RateTestDir =  "RateTests"
        settings = None

        # We ignore errors in this part and keep default name in that case...
        try:
            if os.path.exists(settingsfile):
                with open(settingsfile) as f:
                    settings = f.readlines()[0].strip()
                    RateTestDir = RateTestDir + "_" + settings
        except:
            pass

        targetDir = os.path.join(wwwDir, RateTestDir, dirname)
        os.makedirs(targetDir)
        rateScriptOutputDir = os.path.join(directory, "rateScriptOutput")
        os.makedirs(rateScriptOutputDir)

        subprocess.call(["HLTRateScripts/scripts/analyse.sh", directory + "/", rateScriptOutputDir])
        logname = os.path.join(directory,'run.log')
        tuplename = os.path.join(directory,'tuples.root')
        try:
            for ifile in os.listdir(rateScriptOutputDir):
                shutil.copy(os.path.join(rateScriptOutputDir, ifile), targetDir)
            shutil.copy(logname, targetDir)
            shutil.copy(tuplename , targetDir)
        except:
            print "Error copying log and tuple"

        # LHCBPR_WWW_EOS set to root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/www/
        targetRootEosDir = wwwDirEos + "/" + RateTestDir + "/" + dirname
        try:
            for ifile in os.listdir(rateScriptOutputDir):
                subprocess.call(['xrdcp', rateScriptOutputDir + "/" + ifile, targetRootEosDir + "/" + ifile])
            subprocess.call(['xrdcp', logname, targetRootEosDir + "/run.log"])
            subprocess.call(['xrdcp', tuplename, targetRootEosDir + "/tuples.root"])
        except Exception, ex:
            logging.warning('Error copying html files to eos: %s', ex)

    def collectResultsExt(self, directory, project, version, platform, hostname, startTime, endTime):
        try:
            self._publishStaticHTML(directory, project, version)
        except:
            (e, value, trace) = sys.exc_info()
            print "Could not publish the static HTML", e, value
            import traceback
            traceback.print_tb(trace)

        #Now parsing the logs
        self._parseHLTRateLog(os.path.join(directory,'run.log'), directory)



    def collectResults(self, directory):
        self._parseHLTRateLog(os.path.join(directory,'run.log'), directory)
