import os
import re
import sys
import subprocess
import glob
from BaseHandler import BaseHandler

class UpgradePrCheckerHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()

    def extractPerf(self, infile, trackingtype="Velo", trackingcat="07"):
        filetoread = open(infile,'r')
        foundtrackingtype = False
        foundtrackingcategory = False
        foundfakestracks = False
        for line in filetoread:
            if not foundtrackingtype:
                if line.find('**** '+trackingtype) > -1:
                    foundtrackingtype = True
                    if line.find("ghosts") > -1 and not foundfakestracks:
                        tracksFound = line.split('tracks including')[0].split(trackingtype)[1].lstrip(' ')
                        fakesFound  = line.split('tracks including')[1].split("ghosts")[0].lstrip(' ')
                        fakeRate    = line.split('tracks including')[1].split("ghosts")[1].split('[')[1].split(']')[0].lstrip(' ').rstrip(' %')
                        print "Nb. Tracks Found = "+tracksFound
                        print "Nb. Fakes  Found = "+fakesFound+ " [ "+fakeRate +" %]"
                        #print 'Velo tracking found =',line.split('tracks including')[0].split(trackingtype)[1].lstrip(' ')
                        foundfakestracks = True
            else:
                if line.find(trackingcat) > -1:
                    efficiency = float(line.split('[')[1].split(']')[0].lstrip(' ').rstrip(' %'))
                    clone_rate = 100.0-float(line.split('purity:')[1].split('%')[0].lstrip(' '))
                    print 'efficiency = ', efficiency
                    print 'clone rate  = ', clone_rate
                    foundtrackingcategory = True
                    break

        if not foundtrackingcategory or not foundfakestracks:
            return None
        else:
            return (efficiency, fakeRate, clone_rate)

    def collectResultsExt(self, directory, project, version, platform, hostname, startTime, endTime):

        # get efficiency and fake rate from PRChecker log
        efficiency, fake_rate, clone_rate = self.extractPerf(os.path.join(directory, "run.log"))

        # save floats and string with path to plot
        self.saveFloat("efficiency",
                       efficiency,
                       description="efficiency",
                       group="performance")
        self.saveFloat("fake_rate",
                       fake_rate,
                       description="fake rate",
                       group="performance")
        self.saveFloat("clone_rate",
                       clone_rate,
                       description="clone rate",
                       group="performance")

        # send plot to eos  as html
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception("No web dir on EOS defined, will not run extraction")
        else:
            dirname = "PrChecker_" + str(version) + "_" + startTime.replace(' ', '_')
            html_code = "<html>"\
                        "<head></head> "\
                        "<body> "\
                        "<p>"+str(version)+"</p>"\
                        "<ul>"\
                        "  <li>Efficiency = "+str(efficiency)+" </li>"\
                        "  <li>Fake rate = "+str(fake_rate)+"</li>"\
                        "  <li>Clone rate = "+str(clone_rate)+"</li>"\
                        "</ul>"\
                        "</body>"\
                        "</html>"

            # # it's assumed that throughput test runs first
            # with open("index.html", "r") as html_file:
            #     html_code = html_file.read()
            #     html_code = html_code.replace("calculating efficiency", str(efficiency))
            #     html_code = html_code.replace("calculating fake_rate", str(fake_rate))
            with open("index.html", "w") as html_file:
                html_file.write(html_code)

            # os.makedirs(dirname)
            # LHCBPR_WWW_EOS should be set to root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/UpgradeVelo/www/
            # for test: root://eoslhcb.cern.ch//eos/lhcb/user/m/maszyman/www
            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            try:
                subprocess.call(['xrdcp', '-f', 'index.html', targetRootEosDir + "/index.html"])
            except Exception, ex:
                logging.warning('Error copying html files to eos: %s', ex)
