import ROOT
import os
import re
from BaseHandler import BaseHandler

class GaussMemHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()
        self.finished = False
        self.results = []

    def collectResults(self,directory):
        l = self.findHistoFile(directory)
        if len(l) != 1:
            raise Exception("Could not locate just 1 histo file, found:" + str(l))

        f = ROOT.TFile(os.path.join(directory, l[0]))
        gaussGenTotal = f.Get("GaussGen.GaussGenMemory/Total Memory [MB]")
        gaussGenDelta = f.Get("GaussGen.GaussGenMemory/Delta Memory [MB]")
        mainEventGaussSimTotal = f.Get("MainEventGaussSim.MainEventGaussSimMemory/Total Memory [MB]")
        mainEventGaussSimDelta = f.Get("MainEventGaussSim.MainEventGaussSimMemory/Delta Memory [MB]")

        self.saveFloat("TotalMemoryGaussGen", gaussGenTotal.GetMean(), "Total Memory [MB]", "Memory");
        self.saveFloat("DeltaMemoryGaussGen", gaussGenDelta.GetMean(), "TotalDelta Memory [MB]", "Memory");
        self.saveFloat("TotalMemoryMainEventGaussSim", mainEventGaussSimTotal.GetMean(), "Total Memory [MB]", "Memory");
        self.saveFloat("DeltaMemoryMainEventGaussSim", mainEventGaussSimDelta.GetMean(), "TotalDelta Memory [MB]", "Memory");

    def findHistoFile(self, dir):
        return [f for f in os.listdir(dir) if re.match("Gauss.*histos.root", f)]
