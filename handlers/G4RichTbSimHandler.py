import os
from BaseHandler import BaseHandler


class G4RichTbSimHandler(BaseHandler):

    def collectResults(self, directory):
        mc_histos_file = os.path.join(directory, 'G4RichTbSimHTestOutput', 'RichTbSim_MC_Histograms.root')
        self.saveFile('G4RichTbSim_MCHistograms', mc_histos_file)
