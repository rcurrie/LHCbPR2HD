import os
import re
import sys
import subprocess
import glob
from BaseHandler import BaseHandler

class UpgradeThroughputHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(self, directory, project, version, platform, hostname, startTime, endTime):
        # clone lhcb-benchmark scripts
        if not os.path.exists("lhcb-benchmark-scripts"):
            subprocess.call("git clone --quiet ssh://git@gitlab.cern.ch:7999/maszyman/lhcb-benchmark-scripts.git",
                                 shell=True)
        else:
            subprocess.call(["git",
                             "--git-dir=./lhcb-benchmark-scripts/.git",
                             "pull","origin","master"])

        # run extract.py from directory
        subprocess.call(["lb-run",
                         "LHCb/latest",
                         "python",
                         "lhcb-benchmark-scripts/extract.py",
                         directory+"/output"])

        # run plotSpeedUp.py
        max_throughput_nt = subprocess.check_output(["lb-run",
                                                     "LHCb/latest",
                                                     "python",
                                                     "lhcb-benchmark-scripts/plotSpeedup.py",
                                                     "extractedData"])
        max_throughput = float(max_throughput_nt.split(" ")[0])
        max_nt = int(max_throughput_nt.split(" ")[1])
        print("max_throughput = ", max_throughput)
        print("max_nt = ", max_nt)
        self.saveFloat("max_throughput",
                       max_throughput,
                       description="maximum throughput",
                       group="throughput")

        # run plotAlgoUsage.py on a csv file which has max throughput
        subprocess.call(["lb-run",
                         "LHCb/latest",
                         "python",
                         "lhcb-benchmark-scripts/plotAlgoUsage.py",
                         glob.glob(directory+"/output/*"+str(max_nt)+"t*csv")[0]])

        efficiency = None
        fake_rate = None

        # send plot to eos  as html
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception("No web dir on EOS defined, will not run extraction")
        else:
            dirname = "Throughput_" + str(version) + "_" + startTime.replace(' ', '_')
            html_code = "<html>"\
                        "<head></head> "\
                        "<body> "\
                        "<p>"+str(version)+"</p>"\
                        "<ul>"\
                        "  <li>Maximum throughput = "+str(max_throughput)+"</li>"\
                        "</ul>"\
                        "<img src="'throughput.png'">"\
                        "<img src="'algousage.png'">"\
                        "</body>"\
                        "</html>"

            with open("index.html", "w") as html_file:
                html_file.write(html_code)

            # os.makedirs(dirname)
            # LHCBPR_WWW_EOS should be set to root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/UpgradeVelo/www/
            # for test: root://eoslhcb.cern.ch//eos/lhcb/user/m/maszyman/www
            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            try:
                subprocess.call(['xrdcp', 'throughput.png', targetRootEosDir + "/throughput.png"])
                subprocess.call(['xrdcp', 'algousage.png', targetRootEosDir + "/algousage.png"])
                subprocess.call(['xrdcp', 'index.html', targetRootEosDir + "/index.html"])
            except Exception, ex:
                logging.warning('Error copying html files to eos: %s', ex)

            self.saveString("throughput",
                           "cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/"+dirname+"throughput.png",
                           description="link to throughput vs parallelisation plot",
                           group="performance")
            self.saveString("algousage",
                           "cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/"+dirname+"algousage.png",
                           description="link to algo usage plot",
                           group="performance")
