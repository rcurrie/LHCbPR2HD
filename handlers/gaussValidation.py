import re, sys, os, shutil, json, glob
from BaseHandler import BaseHandler
from xml.etree.ElementTree import ElementTree
import lxml.etree as etree

# True or False
global DEBUG 
DEBUG = True

#################################################################################
# search "pattern" on "line", if not found return "default"
# if DEBUG and "name" are define print: "name: value"  
def grepPattern(pattern, line, default = None, name = ""):
  result = default
  resultobject = re.search( pattern, line )
  if ( resultobject != None ):
    tmp = resultobject.groups()
    if ( len(tmp) == 1 ):
      result = tmp[0]
    else:
      result = tmp
    if (DEBUG and name):
      print "[grepPattern] %s: %s" % (name, result)
  else:
    print "WARNING: attribute %s was not found!" % name
  return result

#################################################################################

class GaussLogFile:
  def __init__(self,N):
    self.fileName = N


	# parse the xml log file and returns a dictionary with INT and FLOAT variables
  def parseXmlLog(self, filename):
    # the result dictionary initialization
    result = {}
    result["Int"] = {} # int variables
    result["Float"] = {} # float variables
    result["String_fraction"] = {} # variables in format number (value +/- err)
    result["String_efficiency"] = {} # variables in format number (value +/- err)
    result["String_gen"] = {} # general informations

    try:
      fd = open(filename)
      parser = etree.XMLParser(recover=True)
      tree   = etree.parse(fd, parser)
    except IOError:
      print "WARNING! File GeneratorLog.xml was not set!"
      return False
    
    if DEBUG:
      print "Parsing GeneratorLog.xml..."

    root = tree.getroot() 
    
    # first save all the couples name = value
    for counter in root.findall('counter'):
      value = counter.find('value').text
      name = counter.get('name')
      if DEBUG:
        print name, value
        
      # save all values in the dictionary
      result["Int"][name] = value
      
    # look at the crosssection part      
    for crosssection in root.findall('crosssection'):
      description = crosssection.find('description').text
      generated = crosssection.find('generated').text
      value = crosssection.find('value').text
      id = crosssection.get('id')
      result["Float"][description] = value
      if DEBUG:
        print "id", id,  description, value

    # look at the fraction part
    for fraction in root.findall('fraction'):
      name = fraction.get('name')
      number = fraction.find('number').text
      value = fraction.find('value').text
      error = fraction.find('error').text
      result["String_fraction"][name]= number + "(" + value + "+/-" + error + ")"

      if DEBUG:
          print  name, number, value, error, result["String_fraction"][name]
      
    # efficiencies
    for efficiency in root.findall('efficiency'):
      name = efficiency.get('name')
      before = efficiency.find('before').text
      after = efficiency.find('after').text
      value = efficiency.find('value').text
      error = efficiency.find('error').text

      result["String_efficiency"][name]= before + "/" + after + "(" + value + "+/-" + error + ")"

      if DEBUG:
          print  name, before, after, value, error, result["String_efficiency"][name]

    ## general information
    if root.find('generator') is not None:
      result["String_gen"]['generator'] = root.find('generator').text
    else:
      result["String_gen"]['generator'] = "N/A"

    if root.find('eventType') is not None:
      result["String_gen"]['eventType'] = root.find('eventType').text 
    else:  
     result["String_gen"]['eventType']  = "N/A"
     
    if root.find('method') is not None:
      result["String_gen"]['method'] = root.find('method').text
    else:
      result["String_gen"]['method'] = "N/A/"  

    return result
  
  def computeQuantities(self):
    if DEBUG:
      print "Log file name = ", self.fileName
      
    # read logfile in one shoot  
    f = open(self.fileName)
    logfile = f.read()
    f.close()
   
#################################################
#Version information                            #
#################################################   
   
    self.EventType = grepPattern('Requested to generate EventType (\d+)', logfile, 0, 'EventType')
    self.GaussVersion = grepPattern( 'Welcome to Gauss version (\S+)', logfile, "", 'GaussVersion')
    self.PythiaVersion = grepPattern( 'This is PYTHIA version (\S+)', logfile, "", 'PythiaVersion')  
    self.GeantVersion = grepPattern( 'Geant4 version Name: *(\S+)  *\S+', logfile, "", 'GeantVersion')    
    self.DDDBVersion = grepPattern( 'DDDB *INFO Using TAG (\S+)', logfile, "", 'DDDBVersion')
    self.SIMCONDVersion = grepPattern( 'SIMCOND *INFO Using TAG (\S+)', logfile, "", 'SIMCONDVersion')

#################################################
#VeloGaussMoni               INFO               #
#################################################

    self.MCHits = grepPattern( 'VeloGaussMoni *INFO \| Number of MCHits\/Event: *(\S+)', logfile, "", 'MCHits')
    self.PileUpMCHits = grepPattern( 'VeloGaussMoni *INFO \| Number of PileUpMCHits\/Event: *(\S+)', logfile, "", 'PileUpMCHits')

#################################################
#TTHitMonitor               INFO *** Summary ***#
#################################################

    self.TTHit_Hits = grepPattern( 'TTHitMonitor *INFO #hits per event: (\S+)', logfile, "", 'TTHit_Hits')
    self.TTHit_BetaGamma = grepPattern( 'TTHitMonitor *INFO Mean beta \* gamma: (\S+)', logfile, "", 'TTHit_BetaGamma')
    self.TTHit_DepCharge = grepPattern( 'TTHitMonitor *INFO Most Probable deposited charge: (\S+)', logfile, "", 'TTHit_DepCharge')
    self.TTHit_HalfSampleWidth = grepPattern( 'TTHitMonitor *INFO Half Sample width (\S+)', logfile, "", 'TTHit_HalfSampleWidth')

#################################################
#ITHitMonitor               INFO *** Summary ***#
#################################################

    self.ITHit_Hits = grepPattern( 'ITHitMonitor *INFO #hits per event: (\S+)', logfile, "", 'ITHit_Hits')
    self.ITHit_BetaGamma = grepPattern( 'ITHitMonitor *INFO Mean beta \* gamma: (\S+)', logfile, "", 'ITHit_BetaGamma')    
    self.ITHit_DepCharge = grepPattern( 'ITHitMonitor  *INFO Most Probable deposited charge: (\S+)', logfile, "", 'ITHit_DepCharge')
    self.ITHit_HalfSampleWidth = grepPattern( 'ITHitMonitor *INFO Half Sample width (\S+)', logfile, "", 'ITHit_HalfSampleWidth')

#################################################
#OTHitMonitor               INFO *** Summary ***#
#################################################

    self.OTHit_Hits = grepPattern( 'OTHitMonitor *INFO #hits per event: (\S+)', logfile, "", 'OTHit_Hits')
    self.OTHit_BetaGamma = grepPattern( 'OTHitMonitor  *INFO Mean beta \* gamma: (\S+)', logfile, "", 'OTHit_BetaGamma')
    self.OTHit_DepCharge = grepPattern( 'OTHitMonitor *INFO Most Probable deposited charge: (\S+)', logfile, "", 'OTHit_DepCharge')
    self.OTHit_HalfSampleWidth = grepPattern( 'OTHitMonitor *INFO Half Sample width (\S+)', logfile, "", 'OTHit_HalfSampleWidth')

#################################################################          
#******Stat******           INFO  The Final stat Table (ordered)#
################################################################

    # the sum is the second value

    self.MCRichTracks = grepPattern( '\**Stat.*INFO *"#MCRichTracks" \| *\d+ \| *(\d+)', logfile, 0, 'MCRichTracks')
    self.MCRichSegment = grepPattern( '\**Stat.*INFO *"#MCRichSegment \| *\d+ \| *(\d+)', logfile, 0, 'MCRichSegment')
    self.Muon_MCHits = grepPattern( '\**Stat.*INFO *"#Muon MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Muon_MCHits')
    self.IT_MCHits = grepPattern( '\**Stat.*INFO *"#IT MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'IT_MCHits')
    self.TT_MCHits = grepPattern( '\**Stat.*INFO *"#TT MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'TT_MCHits')
    self.Hcal_MCHits = grepPattern( '\**Stat.*INFO *"#Hcal MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Hcal_MCHits')
    self.OT_MCHits = grepPattern( '\**Stat.*INFO *"#OT MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'OT_MCHits')
    self.Velo_MCHits = grepPattern( '\**Stat.*INFO *"#Velo MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Velo_MCHits')
    self.Rich2_MCHits = grepPattern( '\**Stat.*INFO *"#Rich2 MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Rich2_MCHits')
    self.Spd_MCHits = grepPattern( '\**Stat.*INFO *"#Spd MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Spd_MCHits')
    self.Rich1_MCHits = grepPattern( '\**Stat.*INFO *"#Rich1 MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Rich1_MCHits')
    self.MCParticles = grepPattern( '\**Stat.*INFO *"#MCParticles" *\| *\d+ \| *(\d+)', logfile, 0, 'MCParticles')
    self.MCVertices = grepPattern( '\**Stat.*INFO *"#MCVertices" *\| *\d+ \| *(\d+)', logfile, 0, 'MCVertices')
    self.Prs_MCHits = grepPattern( '\**Stat.*INFO *"#Prs MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Prs_MCHits')
    self.MCRichOpPhoto = grepPattern( '\**Stat.*INFO *"#MCRichOpPhoto *\| *\d+ \| *(\d+)', logfile, 0, 'MCRichOpPhoto')
    self.Rich_MCHits = grepPattern( '\**Stat.*INFO *"#Rich MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Rich_MCHits')
    self.Ecal_MCHits = grepPattern( '\**Stat.*INFO *"#Ecal MCHits" *\| *\d+ \| *(\d+)', logfile, 0, 'Ecal_MCHits')

#################################################################          
# Muon Monitoring Table                                         #
#################################################################

    (self.R1_M1, self.R1_M2, self.R1_M3, self.R1_M4, self.R1_M5) = grepPattern( 'MuonHitChecker             INFO (\S+) * (\S+) * (\S+) * (\S+) * (\S+) * R1', logfile, (0,0,0,0,0), 'R1')
    (self.R2_M1, self.R2_M2, self.R2_M3, self.R2_M4, self.R2_M5) = grepPattern( 'MuonHitChecker             INFO (\S+) * (\S+) * (\S+) * (\S+) * (\S+) * R2', logfile, (0,0,0,0,0), 'R2')
    (self.R3_M1, self.R3_M2, self.R3_M3, self.R3_M4, self.R3_M5) = grepPattern( 'MuonHitChecker             INFO (\S+) * (\S+) * (\S+) * (\S+) * (\S+) * R3', logfile, (0,0,0,0,0), 'R3') 
    (self.R4_M1, self.R4_M2, self.R4_M3, self.R4_M4, self.R4_M5) = grepPattern( 'MuonHitChecker             INFO (\S+) * (\S+) * (\S+) * (\S+) * (\S+) * R4', logfile, (0,0,0,0,0), 'R4')
    (self.InvRichFlags, self.InvRichFlagsErr) = grepPattern( 'GetRichHits *INFO.*Invalid RICH flags *= *(\S+) *\+\- *(\S+)', logfile, (0, 0), 'InvRichFlags')
    (self.MCRichHitsR1, self.MCRichHitsR1Err, self.MCRichHitsR2, self.MCRichHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*MCRichHits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 = *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'MCRichHits')
    (self.InvRadHitsR1, self.InvRadHitsR1Err, self.InvRadHitsR2, self.InvRadHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*Invalid radiator hits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'InvRadHits')     
    (self.SignalHitsR1, self.SignalHitsR1Err, self.SignalHitsR2, self.SignalHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*Signal Hits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'SignalHits')
    (self.GasQuartzCKHitsR1, self.GasQuartzCKHitsR1Err, self.GasQuartzCKHitsR2, self.GasQuartzCKHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*Gas Quartz CK hits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'GasQuartzCKHits')
    (self.HPDQuartzCKHitsR1, self.HPDQuartzCKHitsR1Err, self.HPDQuartzCKHitsR2, self.HPDQuartzCKHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*HPD Quartz CK hits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'HPDQuartzCKHits')
    (self.NitrogenCKHitsR1, self.NitrogenCKHitsR1Err, self.NitrogenCKHitsR2, self.NitrogenCKHitsR2Err) = \
      grepPattern( 'GetRichHits *INFO.*Nitrogen CK hits *: Rich1 *= *(\S+) \+\- *(\S+).*Rich2 *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0), 'NitrogenCKHits')
    (self.SignalCKAero, self.SignalCKAeroErr, self.SignalCKC4F10, self.SignalCKC4F10Err, self.SignalCKCF4, self.SignalCKCF4Err) = \
      grepPattern( 'GetRichHits *INFO.*Signal CK MCRichHits *: Aero *= *(\S+) \+\- *(\S+).*Rich1Gas *= *(\S+) \+\- *(\S+).*Rich2Gas *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0,0,0), 'SignalCK')
    (self.ScatteredHitsAero, self.ScatteredHitsAeroErr, self.ScatteredHitsC4F10, self.ScatteredHitsC4F10Err, self.ScatteredHitsCF4, self.ScatteredHitsCF4Err) = \
      grepPattern( 'GetRichHits *INFO.*Rayleigh scattered hits *: Aero *= *(\S+) \+\- *(\S+).*Rich1Gas *= *(\S+) \+\- *(\S+).*Rich2Gas *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0,0,0), 'ScatteredHits')
    (self.MCParticleLessHitsAero, self.MCParticleLessHitsAeroErr, self.MCParticleLessHitsC4F10, self.MCParticleLessHitsC4F10Err, self.MCParticleLessHitsCF4, self.MCParticleLessHitsCF4Err) = \
      grepPattern( 'GetRichHits *INFO.*MCParticle-less hits *: Aero *= *(\S+) \+\- *(\S+).*Rich1Gas *= *(\S+) \+\- *(\S+).*Rich2Gas *= *(\S+) *\+\- *(\S+)', logfile, (0,0,0,0,0,0), 'MCParticleLessHits')
  
class gaussValidation(BaseHandler):
    
    def __init__(self):
        super(self.__class__, self).__init__()
    
    def collectResults(self,directory):
        
        # Informations are stored in two files: run.log and GeneratorLog.xml
        logfile = os.path.join(directory, 'run.log')
        genfile = os.path.join(directory, 'GeneratorLog.xml') 

        # define groups
        grp = {}		
        grp['general'] = "Validation_General"  
        grp['version'] = "Validation_Version"
        grp['time'] = "Validation_Time"
        grp['generator_count'] = "Validation_Generator_counters"
        grp['generator_cross'] = "Validation_Generator_crossSection"
        grp['generator_fraction'] = "Validation_Generator_fraction"
        grp['generator_efficiency'] = "Validation_Generator_efficiency"
        grp['it_ot_tt'] = "Validation_IT_OT_TT"
        grp['velo'] = "Validation_Velo"
        grp['muon'] = "Validation_Muon_detectors"
        grp['rich'] = "Validation_Rich"
        grp['mc_hits'] = "Validation_MC_hits"         

        # first look for the "ROOT" file
        
        rootfile = grepPattern('RootHistSvc\s.*INFO Writing ROOT histograms to: (\S+)' , open(logfile, 'r').read() )
        rootfullname = os.path.join(directory,rootfile)
        
        if os.path.isfile(rootfullname) == 0:
            raise Exception("Could not locate histo file: %s in the given directory"%rootfile)
        
        fileName, fileExtension = os.path.splitext(rootfile)
        self.saveFile("GaussROOTOutput", rootfullname)
        
        TheLog = GaussLogFile( logfile )
        
        # Parse and save the values found in the xml file
        xmllog = TheLog.parseXmlLog(genfile)
        if xmllog:
          for name, value in xmllog["Int"].items():
            self.saveInt(name, value, "", grp['generator_count'])
          for name, value in xmllog["Float"].items():
            self.saveFloat(name, value, "", grp['generator_cross'])
          for name, value in xmllog["String_fraction"].items():
            self.saveString(name, value, "", grp['generator_fraction'])
          for name, value in xmllog["String_efficiency"].items():
            self.saveString(name, value, "", grp['generator_efficiency'])
            
          self.saveString('Generator', xmllog["String_gen"]['generator'], grp['general'])
          self.saveInt('EventType',  xmllog["String_gen"]['eventType'], grp['general'])
          self.saveString('Method', xmllog["String_gen"]['method'], grp['general'])

        # Parse the run.log file
        TheLog.computeQuantities() 

        if not xmllog:
          self.saveInt('EventType', TheLog.EventType, grp['general'])

        self.saveString('GaussVersion', TheLog.GaussVersion, 'Gauss', grp['version'])
        self.saveString('PythiaVersion', TheLog.PythiaVersion, 'Pythia', grp['version'])
        self.saveString('DDDBVersion', TheLog.DDDBVersion, 'DDDB', grp['version'])
        self.saveString('GeantVersion', TheLog.GeantVersion, 'Geant', grp['version'])
        self.saveString('SIMCONDVersion', TheLog.SIMCONDVersion, 'SIMCOND', grp['version'])
        
        self.saveString('MCHits',TheLog.MCHits,'Velo MCHits / Event',grp["velo"])
        self.saveString('PileUpMCHits',TheLog.PileUpMCHits, 'Velo Pile Up MCHits', grp["velo"])
        
        self.saveFloat('TTHits',TheLog.TTHit_Hits, 'TT hits per event', grp["it_ot_tt"])
        self.saveFloat('TTHit_BetaGamma',TheLog.TTHit_BetaGamma, 'TT Mean beta * gamma', grp["it_ot_tt"])
        self.saveFloat('TTHit_DepCharge',TheLog.TTHit_DepCharge, 'TT Most Probable deposited charge', grp["it_ot_tt"])
        self.saveFloat('TTHit_HalfSampleWidth',TheLog.TTHit_HalfSampleWidth, 'TT Half Sample Width ', grp["it_ot_tt"])
        self.saveFloat('ITHits',TheLog.TTHit_HalfSampleWidth, 'IT hits per event', grp["it_ot_tt"])
        self.saveFloat('ITHit_BetaGamma',TheLog.ITHit_BetaGamma, 'IT Mean beta * gamma', grp["it_ot_tt"])
        self.saveFloat('ITHit_DepCharge',TheLog.ITHit_DepCharge, 'IT Most Probable deposited charge', grp["it_ot_tt"])
        self.saveFloat('ITHit_HalfSampleWidth',TheLog.ITHit_HalfSampleWidth, 'IT Half Sample Width', grp["it_ot_tt"])
        self.saveFloat('OTHits',TheLog.OTHit_Hits, 'OT hits per event', grp["it_ot_tt"])
        self.saveFloat('OTHit_BetaGamma',TheLog.OTHit_BetaGamma, 'OT Mean beta * gamma', grp["it_ot_tt"])
        self.saveFloat('OTHit_DepCharge',TheLog.OTHit_DepCharge, 'OT Most Probable deposited charge', grp["it_ot_tt"])
        self.saveFloat('OTHit_HalfSampleWidth',TheLog.OTHit_HalfSampleWidth, 'OT Half Sample Width', grp["it_ot_tt"])

        self.saveInt('MCRichTracks',TheLog.MCRichTracks, 'No MC Rich Tracks', grp["mc_hits"])
        self.saveInt('MCRichSegment',TheLog.MCRichSegment, 'No MC Rich Segment', grp["mc_hits"])
        self.saveInt('Muon_MCHits',TheLog.Muon_MCHits, 'No Muon MCHits', grp["mc_hits"])
        self.saveInt('IT_MCHits',TheLog.IT_MCHits, 'No IT MCHits', grp["mc_hits"])
        self.saveInt('TT_MCHits',TheLog.TT_MCHits, 'No TT MCHits ', grp["mc_hits"])
        self.saveInt('Hcal_MCHits',TheLog.Hcal_MCHits, 'No Hcal MCHits', grp["mc_hits"])
        self.saveInt('OT_MCHits',TheLog.OT_MCHits, 'No OT MCHits', grp["mc_hits"])
        self.saveInt('Velo_MCHits',TheLog.Velo_MCHits, 'No Velo MCHits', grp["mc_hits"])
        self.saveInt('Rich2_MCHits',TheLog.Rich2_MCHits, 'No Rich2 MCHits', grp["mc_hits"])
        self.saveInt('Spd_MCHits',TheLog.Spd_MCHits, 'No Spd MCHits', grp["mc_hits"])
        self.saveInt('Rich1_MCHits',TheLog.Rich1_MCHits, 'No Rich1 MCHits', grp["mc_hits"])
        self.saveInt('MCParticles',TheLog.MCParticles, 'No MC Particles', grp["mc_hits"])
        self.saveInt('MCVertices',TheLog.MCVertices, 'No MC Vertices', grp["mc_hits"])
        self.saveInt('Prs_MCHits',TheLog.Prs_MCHits, 'No Prs MCHits', grp["mc_hits"])
        self.saveInt('MCRichOpPhoto',TheLog.MCRichOpPhoto, 'No Rich Op Photo', grp["mc_hits"])
        self.saveInt('Rich_MCHits',TheLog.Rich_MCHits, 'No Rich MCHits', grp["mc_hits"])
        self.saveInt('Ecal_MCHits',TheLog.Ecal_MCHits, 'No Ecal MCHits', grp["mc_hits"])

        self.saveFloat('R1_M1', TheLog.R1_M1, '', grp["muon"])
        self.saveFloat('R1_M2', TheLog.R1_M2, '', grp["muon"])
        self.saveFloat('R1_M3', TheLog.R1_M3, '', grp["muon"])
        self.saveFloat('R1_M4', TheLog.R1_M4, '', grp["muon"])
        self.saveFloat('R1_M5', TheLog.R1_M5, '', grp["muon"])
        self.saveFloat('R2_M1', TheLog.R2_M1, '', grp["muon"])
        self.saveFloat('R2_M2', TheLog.R2_M2, '', grp["muon"])
        self.saveFloat('R2_M3', TheLog.R2_M3, '', grp["muon"])
        self.saveFloat('R2_M4', TheLog.R2_M4, '', grp["muon"])
        self.saveFloat('R2_M5', TheLog.R2_M5, '', grp["muon"])
        self.saveFloat('R3_M1', TheLog.R3_M1, '', grp["muon"])
        self.saveFloat('R3_M2', TheLog.R3_M2, '', grp["muon"])
        self.saveFloat('R3_M3', TheLog.R3_M3, '', grp["muon"])
        self.saveFloat('R3_M4', TheLog.R3_M4, '', grp["muon"])
        self.saveFloat('R3_M5', TheLog.R3_M5, '', grp["muon"])
        self.saveFloat('R4_M1', TheLog.R4_M1, '', grp["muon"])
        self.saveFloat('R4_M2', TheLog.R4_M2, '', grp["muon"])
        self.saveFloat('R4_M3', TheLog.R4_M3, '', grp["muon"])
        self.saveFloat('R4_M4', TheLog.R4_M4, '', grp["muon"])
        self.saveFloat('R4_M5', TheLog.R4_M5, '', grp["muon"])

        '''
        self.saveString('InvRichFlags (w/e)', TheLog.InvRichFlags + " +/- " + TheLog.InvRichFlagsErr, 'Av. # Invalid RICH flags', grp["rich"])
        self.saveString('MCRichHitsR1 (w/e)', TheLog.MCRichHitsR1 + " +/- " + TheLog.MCRichHitsR1Err, 'Av. # MC Rich1 Hits', grp["rich"])
        self.saveString('MCRichHitsR2 (w/e)', TheLog.MCRichHitsR2 + " +/- " + TheLog.MCRichHitsR2Err, 'AV. # MC Rich2 Hits', grp["rich"])
        self.saveString('InvRadHitsR1 (w/e)', TheLog.InvRadHitsR1 + " +/- " + TheLog.InvRadHitsR1Err, 'Av. # Invalid radiator Rich1 hits', grp["rich"])
        self.saveString('InvRadHitsR2 (w/e)', TheLog.InvRadHitsR2 + " +/- " + TheLog.InvRadHitsR2Err, 'Av. # Invalid radiator Rich2 hits', grp["rich"])
        self.saveString('SignalHitsR1 (w/e)', TheLog.SignalHitsR1 + " +/- " + TheLog.SignalHitsR1Err, 'Av. # Signal Hits Rich1', grp["rich"])
        self.saveString('SignalHitsR2 (w/e)', TheLog.SignalHitsR2 + " +/- " + TheLog.SignalHitsR2Err, 'Av. # Signal Hits Rich2', grp["rich"])
        self.saveString('GasQuartzCKHitsR1 (w/e)', TheLog.GasQuartzCKHitsR1 + " +/- " + TheLog.GasQuartzCKHitsR1Err, 'Av. # Gas Quartz CK Rich1 hits', grp["rich"])
        self.saveString('GasQuartzCKHitsR2 (w/e)', TheLog.GasQuartzCKHitsR2 + " +/- " + TheLog.GasQuartzCKHitsR2Err, 'Av. # Gas Quartz CK Rich2 hits', grp["rich"])
        self.saveString('HPDQuartzCKHitsR1 (w/e)', TheLog.HPDQuartzCKHitsR1 + " +/- " + TheLog.HPDQuartzCKHitsR1Err, 'Av. # HPD Quartz CK Rich1 hits', grp["rich"])
        self.saveString('HPDQuartzCKHitsR2 (w/e)', TheLog.HPDQuartzCKHitsR2 + " +/- " + TheLog.HPDQuartzCKHitsR2Err, 'Av. # HPD Quartz CK Rich2 hits', grp["rich"])
        self.saveString('NitrogenCKHitsR1 (w/e)', TheLog.NitrogenCKHitsR1 + " +/- " + TheLog.NitrogenCKHitsR1Err, 'Av. # Nitrogen CK Rich1 hits', grp["rich"])
        self.saveString('NitrogenCKHitsR2 (w/e)', TheLog.NitrogenCKHitsR2 + " +/- " + TheLog.NitrogenCKHitsR2Err, 'Av. # Nitrogen CK Rich2 hits', grp["rich"])
        self.saveString('SignalCKAero (w/e)', TheLog.SignalCKAero + " +/- " + TheLog.SignalCKAeroErr, 'Av. # Signal CK MCRichHits Aero', grp["rich"])
        self.saveString('SignalCKC4F10', TheLog.SignalCKC4F10 + " +/- " + TheLog.SignalCKC4F10Err, 'Av. # Signal CK MCRichHits Rich1Gas', grp["rich"])
        self.saveString('SignalCKCF4', TheLog.SignalCKCF4 + " +/- " + TheLog.SignalCKCF4Err, 'Av. # Signal CK MCRichHits Rich2Gas', grp["rich"])
        self.saveString('ScatteredHitsAero', TheLog.ScatteredHitsAero + " +/- " + TheLog.ScatteredHitsAeroErr, 'Av. # Rayleigh scattered hits Aero', grp["rich"])
        self.saveString('ScatteredHitsC4F10', TheLog.ScatteredHitsC4F10 + " +/- " + TheLog.ScatteredHitsC4F10Err, 'Av. # Rayleigh scattered hits Rich1Gas', grp["rich"])
        self.saveString('ScatteredHitsCF4', TheLog.ScatteredHitsCF4 + " +/- " + TheLog.ScatteredHitsCF4Err, 'Av. # Rayleigh scattered hits Rich2Gas', grp["rich"])
        self.saveString('MCParticleLessHitsAero', TheLog.MCParticleLessHitsAero + " +/- " + TheLog.MCParticleLessHitsAeroErr, 'Av. # MCParticle-less hits Aero', grp["rich"])
        self.saveString('MCParticleLessHitsC4F10', TheLog.MCParticleLessHitsC4F10 + " +/- " + TheLog.MCParticleLessHitsC4F10Err, 'Av. # MCParticle-less hits Rich1Gas', grp["rich"])
        self.saveString('MCParticleLessHitsCF4', TheLog.MCParticleLessHitsCF4 + " +/- " + TheLog.MCParticleLessHitsCF4Err, 'Av. # MCParticle-less hits Rich2Gas', grp["rich"])
        '''

        self.saveFloat('InvRichFlags', TheLog.InvRichFlags, 'Av. # Invalid RICH flags', grp["rich"])
        self.saveFloat('InvRichFlagsErr', TheLog.InvRichFlagsErr, 'Invalid RICH flags error', grp["rich"])
        self.saveFloat('MCRichHitsR1', TheLog.MCRichHitsR1, 'Av. # MC Rich1 Hits', grp["rich"])
        self.saveFloat('MCRichHitsR1Err', TheLog.MCRichHitsR1Err, 'MC Rich1 Hits error', grp["rich"])
        self.saveFloat('MCRichHitsR2', TheLog.MCRichHitsR2, 'AV. # MC Rich2 Hits', grp["rich"])
        self.saveFloat('MCRichHitsR2Err', TheLog.MCRichHitsR2Err, 'MC Rich2 Hits error', grp["rich"])
        self.saveFloat('InvRadHitsR1', TheLog.InvRadHitsR1, 'Av. # Invalid radiator Rich1 hits', grp["rich"])
        self.saveFloat('InvRadHitsR1Err', TheLog.InvRadHitsR1Err, 'Av. # Invalid radiator Rich1 hits error', grp["rich"])
        self.saveFloat('InvRadHitsR2', TheLog.InvRadHitsR2, 'Av. # Invalid radiator Rich1 hits', grp["rich"])
        self.saveFloat('InvRadHitsR2Err', TheLog.InvRadHitsR2Err, 'Av. # Invalid radiator Rich1 hits error', grp["rich"])
        self.saveFloat('SignalHitsR1', TheLog.SignalHitsR1, 'Av. # Signal Hits Rich1', grp["rich"])
        self.saveFloat('SignalHitsR1Err', TheLog.SignalHitsR1Err, 'Av. # Signal Hits Rich1 error', grp["rich"])
        self.saveFloat('SignalHitsR2', TheLog.SignalHitsR2, 'Av. # Signal Hits Rich2', grp["rich"])
        self.saveFloat('SignalHitsR2Err', TheLog.SignalHitsR2Err, 'Av. # Signal Hits Rich2 error', grp["rich"])
        self.saveFloat('GasQuartzCKHitsR1', TheLog.GasQuartzCKHitsR1, 'Av. # Gas Quartz CK Rich1 hits', grp["rich"])
        self.saveFloat('GasQuartzCKHitsR1Err', TheLog.GasQuartzCKHitsR1Err, 'Av. # Gas Quartz CK Rich1 hits error', grp["rich"])
        self.saveFloat('GasQuartzCKHitsR2', TheLog.GasQuartzCKHitsR2, 'Av. # Gas Quartz CK Rich2 hits', grp["rich"])
        self.saveFloat('GasQuartzCKHitsR2Err', TheLog.GasQuartzCKHitsR2Err, 'Av. # Gas Quartz CK Rich2 hits error', grp["rich"])
        self.saveFloat('HPDQuartzCKHitsR1', TheLog.HPDQuartzCKHitsR1, 'Av. # HPD Quartz CK Rich1 hits', grp["rich"])
        self.saveFloat('HPDQuartzCKHitsR1Err', TheLog.HPDQuartzCKHitsR1Err, 'Av. # HPD Quartz CK Rich1 hits error', grp["rich"])
        self.saveFloat('HPDQuartzCKHitsR2', TheLog.HPDQuartzCKHitsR1, 'Av. # HPD Quartz CK Rich2 hits', grp["rich"])
        self.saveFloat('HPDQuartzCKHitsR2Err', TheLog.HPDQuartzCKHitsR1Err, 'Av. # HPD Quartz CK Rich2 hits error', grp["rich"])
        self.saveFloat('NitrogenCKHitsR1', TheLog.NitrogenCKHitsR1, 'Av. # Nitrogen CK Rich1 hits', grp["rich"])
        self.saveFloat('NitrogenCKHitsR1Err', TheLog.NitrogenCKHitsR1Err, 'Av. # Nitrogen CK Rich1 hits error', grp["rich"])
        self.saveFloat('NitrogenCKHitsR2', TheLog.NitrogenCKHitsR2, 'Av. # Nitrogen CK Rich2 hits', grp["rich"])
        self.saveFloat('NitrogenCKHitsR2Err', TheLog.NitrogenCKHitsR2Err, 'Av. # Nitrogen CK Rich2 hits error', grp["rich"])
        self.saveFloat('SignalCKAero', TheLog.SignalCKAero, 'Av. # Signal CK MCRichHits Aero', grp["rich"])
        self.saveFloat('SignalCKAeroErr', TheLog.SignalCKAeroErr, 'Av. # Signal CK MCRichHits Aero error', grp["rich"])
        self.saveFloat('SignalCKC4F10', TheLog.SignalCKC4F10, 'Av. # Signal CK MCRichHits Rich1Gas', grp["rich"])
        self.saveFloat('SignalCKC4F10Err', TheLog.SignalCKC4F10Err, 'Av. # Signal CK MCRichHits Rich1Gas error', grp["rich"])
        self.saveFloat('SignalCKCF4', TheLog.SignalCKCF4, 'Av. # Signal CK MCRichHits Rich2Gas', grp["rich"])
        self.saveFloat('SignalCKCF4Err', TheLog.SignalCKCF4Err, 'Av. # Signal CK MCRichHits Rich2Gas error', grp["rich"])
        self.saveFloat('ScatteredHitsAero', TheLog.ScatteredHitsAero, 'Av. # Rayleigh scattered hits Aero', grp["rich"])
        self.saveFloat('ScatteredHitsAeroErr', TheLog.ScatteredHitsAeroErr, 'Av. # Rayleigh scattered hits Aero error', grp["rich"])
        self.saveFloat('ScatteredHitsC4F10', TheLog.ScatteredHitsC4F10, 'Av. # Rayleigh scattered hits Rich1Gas', grp["rich"])
        self.saveFloat('ScatteredHitsC4F10Err', TheLog.ScatteredHitsC4F10Err, 'Av. # Rayleigh scattered hits Rich1Gas error', grp["rich"])
        self.saveFloat('ScatteredHitsCF4', TheLog.ScatteredHitsCF4, 'Av. # Rayleigh scattered hits Rich2Gas', grp["rich"])
        self.saveFloat('ScatteredHitsCF4Err', TheLog.ScatteredHitsCF4Err, 'Av. # Rayleigh scattered hits Rich2Gas error', grp["rich"])
        self.saveFloat('MCParticleLessHitsAero', TheLog.MCParticleLessHitsAero, 'Av. # MCParticle-less hits Aero', grp["rich"])
        self.saveFloat('MCParticleLessHitsAeroErr', TheLog.MCParticleLessHitsAeroErr, 'Av. # MCParticle-less hits Aero error', grp["rich"])
        self.saveFloat('MCParticleLessHitsC4F10', TheLog.MCParticleLessHitsC4F10, 'Av. # MCParticle-less hits Rich1Gas', grp["rich"])
        self.saveFloat('MCParticleLessHitsC4F10Err', TheLog.MCParticleLessHitsC4F10Err, 'Av. # MCParticle-less hits Rich1Gas error', grp["rich"])
        self.saveFloat('MCParticleLessHitsCF4', TheLog.MCParticleLessHitsCF4, 'Av. # MCParticle-less hits Rich2Gas', grp["rich"])
        self.saveFloat('MCParticleLessHitsCF4Err', TheLog.MCParticleLessHitsCF4Err, 'Av. # MCParticle-less hits Rich2Gas error', grp["rich"])
        
        
