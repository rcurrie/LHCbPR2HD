import json, os, datetime, subprocess
from BaseHandler import BaseHandler

from hlt.EventSizeParser import eventSizeParser
from hlt.HLT1TupleParser import ParseHLT1Output
from hlt.HLT2TupleParser import ParseHLT2Output

class HLTJSONHandler(BaseHandler):
    """
    This is the JSON based handler for the HLT data
    This is intended to parse the tuples.root file from a MooreRateTest
    Most of the actual parsing of these input tuples is done in supporting code
    which can be found in the 'hlt' folder
    """

    def __init__(self):
        super(self.__class__, self).__init__()
        self.finished = False
        self.results = []

    def collectResults(self, directory):
        """
        This is the method for parsing the results from
        HLT Rate Tests.
        This has support for parsing the output tuple from a split test or
        from a single tuple from a combined test.
        """    
   
        hlt1_results = os.path.join(directory, "tuples_hlt1.root")
        hlt2_results = os.path.join(directory, "tuples_hlt2.root")

        if not os.path.isfile(hlt1_results) and not os.path.isfile(hlt2_results):
            default_results = os.path.join(directory, 'tuples.root')
            if os.path.isfile(default_results):
                hlt1_results = default_results
                hlt2_results = default_results
            else:
                return

        results_EventSize = eventSizeParser(hlt1_results)

        results_hlt1 = ParseHLT1Output(hlt1_results)
        results_hlt2 = ParseHLT2Output(hlt1_results, hlt2_results)

        eventSizes = results_EventSize['eventSizes']
        for k, v in eventSizes.iteritems():
            self.saveJSON( 'eventSizes_'+k, v )

        self.saveFile(os.path.basename(results_EventSize['eventFileName']), results_EventSize['eventFileName'])

        hlt1_ByRegex = results_hlt1['ByRegex']
        hlt1_Hlt1Lines = results_hlt1['Hlt1Lines']

        for k, v in hlt1_ByRegex.iteritems():
            self.saveJSON( 'hlt1_ByRegex_'+k, v )
        for k, v in hlt1_Hlt1Lines.iteritems():
            self.saveJSON( 'hlt1_Hlt1Lines_'+k, v )

        hlt2_unaccounted = results_hlt2['unaccounted']
        for k, v in hlt2_unaccounted.iteritems():
            self.saveJSON( 'hlt2_unaccounted_'+k, list(v) )
        hlt2_decisions = results_hlt2['decisions']
        for k, v in hlt2_decisions.iteritems():
            self.saveJSON( 'hlt2_decisions_'+k, v )

        # List
        hlt2_persistRecoLines = results_hlt2['persistRecoLines']
        self.saveJSON( 'hlt2_persistRecoLines_'+k, hlt2_persistRecoLines )

        hlt2_Streams = results_hlt2['Streams']
        for k, v in hlt2_Streams.iteritems():
            for k2, v2 in v.iteritems():
                self.saveJSON( 'hlt2_Streams_'+k+'_'+k2, v2 )

        hlt2_RateMatrix = results_hlt2['RateMatrix']
        hlt2_RateMatrixDict = results_hlt2['RateMatrixDict']
        hlt2_RateMatrixLines = results_hlt2['RateMatrixLines']
        hlt2_RateMatrixLineNames = results_hlt2['RateMatrixLineNames']

        # List
        self.saveJSON( 'hlt2_RateMatrix', hlt2_RateMatrix )
        for k, v in hlt2_RateMatrixDict.iteritems():
            self.saveJSON( 'hlt2_RateMatrixDict_'+k, v )
        self.saveJSON( 'hlt2_RateMatrixLines', hlt2_RateMatrixLines )
        self.saveJSON( 'hlt2_RateMatrixLineNames', hlt2_RateMatrixLineNames )


