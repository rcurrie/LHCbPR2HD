##############################################################################################
## Handler for EM test. The input are the rootfiles that contain the histograms of interest
## @Author: G. Chatzikonstantinidis                                    
## @date:   Last modified 2017-10-02                                   
##############################################################################################

import json, os, re
from glob import glob
from BaseHandler import BaseHandler
           

class EMHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()
        
    def collectResults(self, directory):
        rootFiles = glob(directory+"*.root" if directory.endswith("/") else directory+"/*.root")
        for files in rootFiles:
            self.saveFile(re.sub(".root","",files.split("RootFile",1)[1]), files)
            
