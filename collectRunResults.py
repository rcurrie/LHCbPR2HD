#!/usr/bin/env python

import os
import sys
import subprocess
import inspect
import json
import logging
import uuid
import zipfile
import ntpath
import sendToDB
import argparse
import urllib2
from datetime import (timedelta, datetime, tzinfo)

class FixedOffset(tzinfo):

    """Fixed offset in minutes: `time = utc_time + utc_offset`."""

    def __init__(self, offset):
        self.__offset = timedelta(minutes=offset)
        hours, minutes = divmod(offset, 60)
        # NOTE: the last part is to remind about deprecated POSIX GMT+h timezones
        #  that have the opposite sign in the name;
        #  the corresponding numeric value is not used e.g., no minutes
        self.__name = '<%+03d%02d>%+d' % (hours, minutes, -hours)

    def utcoffset(self, dt=None):
        return self.__offset

    def tzname(self, dt=None):
        return self.__name

    def dst(self, dt=None):
        return timedelta(0)

    def __repr__(self):
        return 'FixedOffset(%d)' % (self.utcoffset().total_seconds() / 60)


def mkdatetime(datestr):
    naive_date_str, _, offset_str = datestr.rpartition(' ')
    naive_dt = datetime.strptime(naive_date_str, '%Y-%m-%d %H:%M:%S')
    offset = int(offset_str[-4:-2]) * 60 + int(offset_str[-2:])
    if offset_str[0] == "-":
        offset = -offset
    dt = naive_dt.replace(tzinfo=FixedOffset(offset))
    return dt


def JobDictionary(hostname, starttime, endtime, cmtconfig, appname, appversion,
                  appversiondatetime, execname, execcontent, optname, optcontent,
                  optstandalone, setupname, setupcontent, status):
    """
    This method creates a dictionary with information about the job (like time_start/end etc)
    which will be added to json_results along with the execution results
    """

    hostDict = {'hostname': hostname, 'cpu_info': '', 'memoryinfo': ''}
    cmtconfigDict = {'platform': cmtconfig}
    DataDict = {
        'HOST': hostDict,
        'CMTCONFIG': cmtconfigDict,
        'time_start': starttime,
        'time_end': endtime,
        'status': status,
        'app_name': appname,
        'app_version': appversion,
        'app_version_datetime': appversiondatetime,
        'exec_name': execname,
        'exec_content': execcontent,
        'opt_name': optname,
        'opt_content': optcontent,
        'opt_standalone': optstandalone,
        'setup_name': setupname,
        'setup_content': setupcontent
    }

    return DataDict

def urlopen(url):
    '''                                                                                                                                     
    Wrapper for urllib2.urlopen to enable or disable SSL verification.                                                                      
    '''
    if sys.version_info >= (2, 7, 9):
        # with Python >= 2.7.9 SSL certificates are validated by default                                                                    
        # but we can ignore them                                                                                                            
        from ssl import SSLContext, PROTOCOL_SSLv23
        return urllib2.urlopen(url, context=SSLContext(PROTOCOL_SSLv23))
    return urllib2.urlopen(url)


def main():
    """The collectRunResults scripts creates the json_results file which contains information about the
    the runned job(platform,host,status etc) along with the execution results, the output(logs, root files,xml files)
     of a job are collected by handlers. Each handler knows which file must parse, so this script imports dynamically
     each handler(from the input handler list, --list-handlers option) and calls the collectResults function, of each handler, and
     passes to the function the directory(the default is the . <-- current directory) to the results(output of the runned job)"""
    # this is used for checking
    outputfile = 'json_results'

    description = """The program needs all the input arguments(options in order to run properly)"""
    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-r', '--results', default=".",
                        help='Directory which contains results, default is the current directory')

    parser.add_argument('--app-name',
                        help='Application name (Brunel, Gauss, Moore, ...)',
                        required=True)
    parser.add_argument('--app-version',
                        help='Application release/build version (v42r0, lhcb-gaudi-header-111,...)',
                        required=True)
    parser.add_argument('--app-version-datetime',
                        help='Application release/build creation time (2015-10-13 11:00:00 +0200)',
                        type=mkdatetime,
                        required=True)
    parser.add_argument('--exec-name',
                        help='Executable name',
                        required=True)
    parser.add_argument('--exec-content',
                        help='Executable command (lb-run, gaudirun.py,...)',
                        required=True)
    parser.add_argument('--opt-name',
                        help='Option name (PRTEST-COLLISION12-1000, PRTEST-Callgrind-300evts,...)',
                        required=True)
    parser.add_argument('--opt-content',
                        help='Option content ("${PRCONFIGOPTS}/Moore/PRTEST-Callgrind-300evts.py",...)',
                        required=True)
    parser.add_argument('--opt-standalone', action='store_true',
                        help='Set flag if option is shell script and not job option',
                        default=False)
    parser.add_argument('--setup-name',
                        help='Setup name (UsePRConfig, UserAreaPRConfig, ...)',
                        required=False)
    parser.add_argument('--setup-content',
                        help='Setup content ("--no-user-area --use PRConfig", "--use PRConfig", ...)',
                        required=False)

    parser.add_argument('-s', '--start-time',
                        dest='startTime', help='The start time of the job.',
                        required=True)
    parser.add_argument('-e', '--end-time',
                        dest="endTime", help="The end time of the job.",
                        required=True)
    parser.add_argument("-p", "--hostname",
                        dest="hostname", help="The name of the host who runned the job.",
                        required=True)
    parser.add_argument("-c", "--platform",
                        dest="platform", help="The platform(cmtconfig) of the job.",
                        required=True)
    parser.add_argument("-l", "--list-handlers",
                        dest="handlers", help="The list of handlers(comma separated.",
                        required=True)
    parser.add_argument("-q", "--quiet", action="store_true",
                        dest="ssss", default=False,
                        help="Just be quiet (do not print info from logger)")
    parser.add_argument("-i", "--count",
                        dest="count", default="1",
                        help="Iteration number of the test in a given jenkins build")
    parser.add_argument("-t", "--status",
                        dest="status", default="0",
                        help="Return code of the test job")
    parser.add_argument("-a", "--auto-send-results", action="store_true",
                        dest="send", default=False,
                        help="Automatically send the zip results to the database and job info to couchdb.")

    options = parser.parse_args()

    fh = logging.FileHandler(os.path.join(options.results,'collect.log'))
    fh.setLevel(level=logging.WARNING)
    ch = logging.StreamHandler()
    ch.setLevel(level=logging.WARNING)

    if not options.ssss:
        logging.root.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    logger = logging.getLogger('collectRunResults.py')
    logger.addHandler(ch)
    logger.addHandler(fh)

    dataDict = JobDictionary(
        options.hostname,
        options.startTime,
        options.endTime,
        options.platform,
        options.app_name,
        options.app_version,
        str(options.app_version_datetime),
        options.exec_name,
        options.exec_content,
        options.opt_name,
        options.opt_content,
        options.opt_standalone,
        options.setup_name,
        options.setup_content,
        options.status
    )

    jobAttributes = []
    handlers_result = []

    # preparing the dashboard for sending the job info to couchdb
    if options.send:
        try:
            from LbNightlyTools.Utils import Dashboard

            dash = Dashboard(credentials=None,
                             flavour='periodic')
            build_id = options.startTime.replace(' ', '_')
            if 'BUILD_ID' in os.environ:
                build_id = os.environ.get('BUILD_ID')
            doc_name = build_id + "." + options.count

            dataDict['handlers_info'] = handlers_result
            dataDict['JobAttributes'] = jobAttributes
            if 'BUILD_URL' in os.environ:
                dataDict['build_url'] = os.environ.get('BUILD_URL') + '/console'

        except Exception, ex:
            logger.warning(
                'Problem with sending information to couchdb: %s', ex
            )

    # no point to run the handlers if the test job failed
    if options.status != "0":
        logger.warning('Test failed, handlers will not be executed')
    else:
        # for each handler in the handlers list
        for handler in options.handlers.split(','):
            module = ''.join(['handlers','.',handler])
            # import the current handler
            try:
                mod = __import__(module, fromlist=[module])
            except ImportError, e:
                logger.exception(
                    'Please check your script or handlers directory: %s', e)
            else:
                # create an instance of a the current handler
                try:
                    klass = getattr(mod, handler)
                    currentHandler = klass()
                except Exception, e:
                    logger.exception(
                        'Could not instantiate handler class.'
                        'Is the class name same as file name? : %s', ex)

                try:
                    # collect results from the given directory(--results-directory, -r)
                    collectresext = getattr(currentHandler, "collectResultsExt", None)
                    if collectresext == None:
                        currentHandler.collectResults(options.results)
                    else:
                        currentHandler.collectResultsExt(options.results,
                                                         project= options.app_name,
                                                         version= options.app_version,
                                                         platform=options.platform,
                                                         hostname=options.hostname,
                                                         startTime=options.startTime,
                                                         endTime=options.endTime)
                except Exception,e:
                    # if any error occurs and the handler fails, inform the user
                    # using the logger and save that the current handler failed
                    logger.exception('Handler exception: %s', e)
                    handlers_result.append(
                        {'handler': handler, 'successful': False})
                else:
                    # in case everything is fine , save that the current handler
                    # worked successfully
                    jobAttributes.extend(currentHandler.getResults())
                    handlers_result.append(
                        {'handler': handler, 'successful': True})

        if not jobAttributes:
            logger.warning(
                'All handlers failed, no results were collected...'
            )
        else:
            unique_results_id = str(uuid.uuid1())
            zipper = zipfile.ZipFile(unique_results_id + '.zip', mode='w')

            for i in range(len(jobAttributes)):
                if jobAttributes[i]['type'] == 'File':
                    head, tail = ntpath.split(jobAttributes[i]['filename'])

                    try:
                        # write to the zip file the root file with a unique name
                        zipper.write(jobAttributes[i]['filename'], tail)
                    except Exception, ex:
                        logger.warning('Could not write the root file to the zip file: %s', ex)
                        pass

                    # update in the json_results the uuid new filename
                    jobAttributes[i]['filename'] = tail

            # add the collected results to the final data dictionary
            dataDict['JobAttributes'] = jobAttributes
            dataDict['results_id'] = unique_results_id
            dataDict['handlers_info'] = handlers_result

            f = open(outputfile, 'w')
            f.write(json.dumps(dataDict))
            f.close()

            # add to the zip results file the json_result file
            zipper.write(outputfile)

            # close the zipfile object
            zipper.close()

            logger.info(unique_results_id + '.zip')

            if options.send:
                logger.info(
                    'Automatically sending the zip results file to the database...')
                lhcbprHandlersDir = os.environ.get("LHCBPR_HANDLERS_PATH")
                if lhcbprHandlersDir == None:
                    raise Exception("No LHCbPR Handlers dir defined, will not send "
                                "the zip results file to the database")

                if not os.path.exists(lhcbprHandlersDir):
                    raise Exception("LHCBPR_HANDLERS_PATH defined but does not exist")

                sendPath = os.path.join(lhcbprHandlersDir, 'sendToDB.py')
                sendLog = subprocess.Popen(['lb-run', '-i', 
                                            '-s', 'X509_CERT_DIR=/etc/grid-security/certificates', 
                                            '-c', 'best',
                                            'LHCbDirac/prod',
                                            sendPath, '-s',
                                            unique_results_id + '.zip', 'False'],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
                stdout, stderr = sendLog.communicate()
                formatterSendToDB = logging.Formatter('%(message)s')
                ch.setFormatter(formatterSendToDB)
                fh.setFormatter(formatterSendToDB)
                if stdout:
                    logger.info(stdout)
                if stderr:
                    logger.info(stderr)
                ch.setFormatter(formatter)
                fh.setFormatter(formatter)

                id_app = 0
                id_opt = 0
                id_ver = 0

                # add to dictionary path to lhcbpr dashboard
                try:
                    active_applications = \
                                          urlopen(
                                              "https://lblhcbpr.cern.ch/api/active/applications/"
                                          ).read()
                    active_options = \
                                     urlopen(
                                         "https://lblhcbpr.cern.ch/api/options/"
                                     ).read()
                    active_versions = \
                                      urlopen(
                                          "https://lblhcbpr.cern.ch/api/versions/"
                                      ).read()


                    # getting the id of the project
                    for active_app in json.loads(active_applications):
                        if active_app['name'] == (options.app_name).upper():
                            id_app = active_app['id']
                            break

                    # getting the id of the test options
                    act_opt = json.loads(active_options)
                    while True:
                        for opt in act_opt['results']:
                            if opt['description'] == options.opt_name:
                                id_opt = opt['id']
                                break
                        if id_opt > 0 or not act_opt['next']:
                            break
                        act_opt = json.loads(urlopen(act_opt['next']).read())

                    # getting the id of the versions (nightlies are assumed)
                    act_ver = json.loads(active_versions)
                    while True:
                        for version in act_ver['results']:
                            if version['version'] == options.app_version and \
                               version['application']['name'] == (options.app_name).upper():
                                id_ver = version['id']
                                break
                        if id_ver > 0 or not act_ver['next']:
                            break
                        act_ver = json.loads(urlopen(act_ver['next']).read())

                except Exception, ex:
                    logger.warning('Problems with connection with LHCbPR api: %s', ex)

                if options.app_name == 'MooreOnline':
                    # temporary workaround to have url to MooreOnline test results
                    # while waiting for the analysis module in LHCbPR
                    dataDict['lhcbpr_url'] = "https://cern.ch/lhcbpr-hlt"
                elif options.app_name == 'Brunel' and options.app_version.startswith("lhcb-upgrade-hackathon"):
                    if "prchecker" in options.opt_name.lower():
                        dataDict['lhcbpr_url'] = "https://cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/PrChecker_" + options.app_version + "_" + options.startTime.replace(' ', '_')
                    elif "throughput" in options.opt_name.lower():
                        dataDict['lhcbpr_url'] = "https://cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/Throughput_" + options.app_version + "_" + options.startTime.replace(' ', '_')
                else:
                    dataDict['lhcbpr_url'] = \
                                             "https://lblhcbpr.cern.ch/#/app/jobs/list?apps={0}&options={1}&withNightly&nightlyVersionNumber=100&versions={2}".format(id_app, id_opt, id_ver)

    if options.send:
        try:
            # removing information unnecessary for the couchdb dashboard
            del dataDict['JobAttributes']
            del dataDict['exec_name']
            del dataDict['exec_content']
            del dataDict['setup_name']
            del dataDict['setup_content']
            del dataDict['opt_standalone']
            # updating the entry
            dash.update(doc_name, dataDict)
        except Exception, ex:
            logger.warning(
                'Problem with sending information to couchdb: %s', ex
            )

if __name__ == '__main__':
    main()
